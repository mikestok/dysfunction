defmodule Mix.Tasks.Compile.Dysfunction do
  @config_path_key :dysfunction_path
  @definition_suffix ".def"
  @elixir_suffix ".ex"

  @moduledoc """
  This wraps `Dysfunction.to_elixir/1` so that `mix` can use it as a compiler.

  It uses `#{inspect(@config_path_key)}` in the mix project config to specify the
  name of the directory for definition (`#{@definition_suffix}`) / generated
  (`#{@elixir_suffix}`) files.

  We don't use manifests to track anything, just `mtime` in the timestamps of the
  files generated from the definition files.

  See https://hexdocs.pm/mix/Mix.Task.Compiler.html
  """

  alias Mix.Task.Compiler
  use Compiler

  @impl Compiler
  def run(_args) do
    source_filenames()
    |> Enum.reject(&fresh_generated_file_exists?/1)
    |> compile()
  end

  @impl Compiler
  def clean do
    generated_filenames()
    |> Enum.each(&remove/1)
  end

  defp compile([]), do: {:noop, []}

  defp compile(filenames) do
    compilation_results =
      filenames
      |> report_compilation()
      |> compile_files()

    compilation_results
    |> Enum.reject(&match?({:ok, _, _}, &1))
    |> Enum.map(&to_diagnostic/1)
    |> wrap_with_error_or_ok()
  end

  defp compile_files(source_filenames), do: Enum.map(source_filenames, &compile_file/1)

  defp compile_file(source_filename) do
    generated_filename = generated_filename(source_filename)

    source_filename
    |> File.read!()
    |> Dysfunction.definition_to_elixir()
    |> case do
      {:ok, elixir_source} ->
        File.write!(generated_filename, elixir_source)
        {:ok, source_filename, generated_filename}

      {:error, _, _, _, _, _} = parsec_error ->
        {:error, source_filename, parsec_error}
    end
  end

  defp wrap_with_error_or_ok(diagnostics) when is_list(diagnostics) do
    if Enum.any?(diagnostics, &(&1.severity == :error)) do
      {:error, diagnostics}
    else
      {:ok, diagnostics}
    end
  end

  defp fresh_generated_file_exists?(source_filename) do
    generated_filename = generated_filename(source_filename)

    case {mtime(source_filename), mtime(generated_filename)} do
      {source_t, :no_file} when is_integer(source_t) ->
        false

      {source_t, generated_t} when is_integer(source_t) and is_integer(generated_t) ->
        source_t < generated_t
    end
  end

  defp mtime(filename) do
    case File.stat(filename, time: :posix) do
      {:ok, %File.Stat{mtime: mtime}} -> mtime
      {:error, :enoent} -> :no_file
    end
  end

  defp report_compilation([_] = list_of_one) do
    IO.puts("Compiling 1 file (#{@definition_suffix})")
    list_of_one
  end

  defp report_compilation(list) when is_list(list) do
    IO.puts("Compiling #{Enum.count(list)} files (#{@definition_suffix})")
    list
  end

  defp remove(filename), do: File.rm!(filename)

  # This *assumes* that all ".ex" files in the "dysfunction_path" are generated
  # files.
  defp generated_filenames do
    dysfunction_path()
    |> Path.join("*.ex")
    |> Path.wildcard()
  end

  defp source_filenames do
    dysfunction_path()
    |> Path.join("*" <> @definition_suffix)
    |> Path.wildcard()
  end

  defp generated_filename(source_filename) do
    String.replace_suffix(source_filename, @definition_suffix, @elixir_suffix)
  end

  defp to_diagnostic({:error, source_filename, {:error, reason, _, _, {line, column}, _}}) do
    IO.puts("#{source_filename}: line #{line}, column #{column}: error #{reason}")

    %Compiler.Diagnostic{
      compiler_name: "dysfunction",
      file: source_filename,
      position: {line, column, line, column},
      message: reason,
      details: reason,
      severity: :error
    }
  end

  defp dysfunction_path, do: Keyword.fetch!(Mix.Project.config(), :dysfunction_path)
end
