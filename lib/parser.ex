defmodule Dysfunction.Parser.Helpers do
  @moduledoc false

  defmacro string_token(token, tag) when is_binary(token) and is_atom(tag) do
    quote do
      unquote(token)
      |> string()
      |> label(unquote(token))
      |> unwrap_and_tag(unquote(tag))
    end
  end
end

defmodule Dysfunction.Parser do
  @moduledoc """
  Various parsers for the definition strings and their components for testing.
  """
  import Dysfunction.Parser.Helpers
  import NimbleParsec

  @math_italic_small ?𝑎..?𝑧
  @math_italic_small_greek ?𝛼..?𝜔
  @lower_case ?a..?z
  @upper_case ?A..?Z
  @digit ?0..?9

  @word [@upper_case, @lower_case, @digit, ?_]
  @identifier [@math_italic_small, @math_italic_small_greek, @lower_case]

  space = string_token(" ", :space)
  lparen = string_token("(", :lparen)
  rparen = string_token(")", :rparen)
  bare_comma = string_token(",", :bare_comma)
  equals = string_token("=", :equals)
  newline = string_token("\n", :newline)
  for = string_token("for", :for)

  comma = bare_comma |> ignore(optional(space))

  bare_binary_operator =
    ascii_string([?+, ?-, ?*, ?/], 1)
    |> label("binary operator")
    |> unwrap_and_tag(:binary_operator)

  bare_guard_binary_operator =
    choice([
      string("<"),
      string(">"),
      string("and"),
      string("or")
    ])
    |> label("binary operator")
    |> unwrap_and_tag(:binary_operator)

  binary_operator =
    ignore(optional(space))
    |> concat(bare_binary_operator)
    |> ignore(optional(space))

  guard_binary_operator =
    ignore(optional(space))
    |> concat(bare_guard_binary_operator)
    |> ignore(optional(space))

  bare_identifier = utf8_string(@identifier, 1)

  identifier =
    bare_identifier
    |> label("identifier")
    |> unwrap_and_tag(:identifier)

  integer =
    integer(min: 1)
    |> unwrap_and_tag(:integer)

  definition_name =
    ascii_string([@upper_case], 1)
    |> optional(ascii_string(@word, min: 1))
    |> reduce({Enum, :join, []})
    |> label("definition name")
    |> unwrap_and_tag(:definition_name)

  function_name = bare_identifier |> label("function name") |> unwrap_and_tag(:function_name)

  identifier_or_integer = choice([identifier, integer]) |> label("identifier or integer")

  args =
    repeat(identifier_or_integer |> concat(ignore(comma)))
    |> concat(identifier_or_integer)
    |> tag(:args)

  params =
    repeat(parsec(:expression) |> concat(ignore(comma)))
    |> parsec(:expression)
    |> tag(:params)

  declaration =
    function_name
    |> ignore(lparen)
    |> concat(args)
    |> ignore(rparen)
    |> tag(:declaration)

  function_call =
    function_name
    |> ignore(lparen)
    |> concat(params)
    |> ignore(rparen)
    |> tag(:function_call)

  # As we let Elixir worry about the precedence of things we don't need to do
  # the traditional term & factor with addition and multiplication...

  term =
    choice([
      lparen |> concat(parsec(:expression)) |> concat(rparen),
      function_call,
      identifier_or_integer
    ])

  defparsecp(
    :expression,
    choice([
      term |> concat(binary_operator) |> concat(parsec(:expression)),
      term
    ])
    |> label("expression")
    |> tag(:expression)
  )

  defparsecp(
    :guard_expression,
    choice([
      identifier_or_integer
      |> concat(guard_binary_operator)
      |> concat(parsec(:guard_expression)),
      identifier_or_integer
    ])
    |> label("expression")
    |> tag(:expression)
  )

  body = parsec(:expression) |> tag(:body)

  clauses =
    parsec(:clause)
    |> times(min: 1)
    |> label("clauses")
    |> tag(:clauses)

  guard =
    ignore(space)
    |> ignore(for)
    |> ignore(space)
    |> concat(parsec(:guard_expression))
    |> tag(:guard)
    |> label("guard")

  defparsec(
    :clause,
    declaration
    |> ignore(optional(space))
    |> ignore(equals)
    |> ignore(optional(space))
    |> concat(body)
    |> optional(guard)
    |> ignore(newline)
    |> tag(:clause)
  )

  defparsec(
    :definition,
    definition_name
    |> ignore(newline)
    |> ignore(newline)
    |> concat(clauses)
    |> eos()
    |> label("definition")
    |> tag(:definition)
  )
end
