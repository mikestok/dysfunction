defmodule Dysfunction.Clause do
  @moduledoc false

  @doc """
  Given a parsed clause turn it into valid elixir
  """
  def to_elixir(clause) do
    """
    def #{clause[:declaration][:function_name]}(#{render_args(clause)})#{
      render_guard(clause[:guard])
    } do
      #{render(clause[:body][:expression], "")}
    end
    """
  end

  defp render_args(clause) do
    used_identifiers =
      MapSet.new()
      |> find_and_add_identifiers(clause[:body][:expression])
      |> find_and_add_identifiers(clause[:guard])

    clause[:declaration][:args]
    |> Enum.map(&render_arg(&1, used_identifiers))
    |> Enum.join(", ")
  end

  defp find_and_add_identifiers(acc, {:identifier, identifier}) do
    MapSet.put(acc, identifier)
  end

  defp find_and_add_identifiers(acc, parts) when is_list(parts) do
    Enum.reduce(parts, acc, &find_and_add_identifiers(&2, &1))
  end

  defp find_and_add_identifiers(acc, {_, part}) do
    find_and_add_identifiers(acc, part)
  end

  defp find_and_add_identifiers(acc, _), do: acc

  defp render_arg({:identifier, identifier} = arg, used_identifiers) do
    prefix = if identifier in used_identifiers, do: "", else: "_"

    render(arg, prefix)
  end

  defp render_arg(arg, _), do: render(arg, "")

  defp render_guard(nil), do: ""

  defp render_guard(guard) do
    render(guard, " when ")
  end

  # yes, these should really go elsewhere, but for now...

  defp render(components, acc) when is_list(components) do
    Enum.reduce(components, acc, fn component, acc -> render(component, acc) end)
  end

  defp render({:expression, components}, acc), do: render(components, acc)

  defp render({:identifier, identifier}, acc), do: acc <> identifier

  defp render({:integer, integer}, acc), do: acc <> to_string(integer)

  defp render({:binary_operator, operator}, acc), do: acc <> " #{operator} "

  defp render({:function_call, call}, acc) do
    rendered_args =
      call[:params]
      |> Enum.map(&render(&1, ""))
      |> Enum.join(", ")

    acc <> call[:function_name] <> "(" <> rendered_args <> ")"
  end

  defp render(x, acc) do
    acc <> inspect(x, label: "caluse's catch all render", limit: :infinity)
  end
end
