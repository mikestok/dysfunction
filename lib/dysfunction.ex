defmodule Dysfunction do
  @moduledoc """
  Translate a function definition to the equivalent Elixir code.
  """

  alias Dysfunction.Definition
  alias Dysfunction.Parser

  @doc """
  We just pass through the Nimble Parsec error tuple for the time being.
  """
  @type error_tuple ::
          {:error, String.t(), String.t(), map, {non_neg_integer, non_neg_integer},
           non_neg_integer}

  @spec definition_to_elixir(String.t()) :: {:ok, String.t()} | error_tuple
  def definition_to_elixir(string) when is_binary(string) do
    string
    |> Parser.definition()
    |> case do
      {:ok, [definition: definition], "", _, _, _} ->
        {:ok, parsed_definition_to_source(definition)}

      {:error, _, _, _, _, _} = error ->
        error
    end
  end

  defp parsed_definition_to_source(definition) do
    definition
    |> Definition.to_elixir()
    |> Code.format_string!()
    |> Enum.join()
    |> (&(&1 <> "\n")).()
  end
end
