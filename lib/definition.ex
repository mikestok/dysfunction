defmodule Dysfunction.Definition do
  @moduledoc false

  alias Dysfunction.Clause

  @doc """
  Convert a parsed definition to elixir source.
  """
  def to_elixir(parsed) do
    clauses =
      parsed
      |> Keyword.get(:clauses)
      |> Keyword.get_values(:clause)

    [
      "defmodule #{module_name(parsed)} do",
      Enum.map(clauses, &Clause.to_elixir(&1)),
      "end"
    ]
    |> List.flatten()
    |> Enum.join("\n")
  end

  def module_name(parsed), do: Keyword.get(parsed, :definition_name)
end
