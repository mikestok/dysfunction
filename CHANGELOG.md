# v1.0.0

Initial version that works "well enough" for giving a talk.

# v1.1.0

Add the ability to use math small italic (𝑎 to 𝑧) and math small greek
(𝛼 to 𝜔) as identifiers.
