# Dysfunction

This generates Elixir modules in a Mix project from function definition files.

It's intended to be a simple enough compiler to show demonstrate how to use
and integrate a compiler in the spirit of
[leex](http://erlang.org/doc/man/leex.html) or
[yecc](http://erlang.org/doc/man/yecc.html): it generates code which a later
part of the mix compilation compiles.

## Operation

This provides a `mix` compiler which converts `.def` files in a specified
directory into `.ex` (Elixir source) files in the same directory.

A `.def` file contains a definition of a mathematical function, for example:

```
Ackermann

f(m, n, 0) = m + n
f(m, 0, 1) = 0
f(m, 0, 2) = 1
f(m, 0, p) = m for p > 2
f(m, n, p) = f(m, f(m, n - 1, p), p - 1) for n > 0 and p > 0
```

After the project is compiled it will provide the function:

```
iex(1)> Ackermann.f(5, 4, 2)
625
```

## Installation

In `mix.exs`:

```elixir

  @dysfunction_path "defs"

  def project do
    [
      # ...,
      compilers: [:dysfunction | Mix.compilers()],
      elixirc_paths: ["lib", @dysfunction_path],
      dysfunction_path: @dysfunction_path
    ]

  def deps do
    [
      # ...,
      {:dysfunction, git: "https://gitlab.com/mikestok/dysfunction.git", tag: "v1.1.1"}
    ]
  end
```

## TODO:

* Get the line number and offset right in the error messages
* Remove nimble parsec's footprint from the package
* Clean up and fix the terminology in the parser
