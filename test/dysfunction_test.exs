defmodule DysfunctionTest do
  use ExUnit.Case
  doctest Dysfunction

  describe "Dysfunction.definition_to_elixir/1" do
    test "identity" do
      source = """
      Identity

      f(x) = x
      """

      expected = """
      defmodule Identity do
        def f(x) do
          x
        end
      end
      """

      assert Dysfunction.definition_to_elixir(source) == {:ok, expected}
    end

    test "error return" do
      source = """
      Identity

      f(x) = x ~ x
      """

      assert {:error, _, _, _, _, _} = Dysfunction.definition_to_elixir(source)
    end

    test "utf 8" do
      source = """
      AckermannClause

      𝜑(𝑚, 𝑛, 0) = 𝑚 + 𝑛
      """

      expected = """
      defmodule AckermannClause do
        def 𝜑(𝑚, 𝑛, 0) do
          𝑚 + 𝑛
        end
      end
      """

      assert Dysfunction.definition_to_elixir(source) == {:ok, expected}
    end
  end
end
